

public class Repair {
	private String address;
	private String id;
	private String long_phone;
	private String name;
	private String question;
	private String short_phone;
	private String state;
	private String timestamp;
	private String type;
	
	public Repair(String address,String id,
			String long_phone,String name,String question,String short_phone,
			String state,String timestamp,String type) {
		this.address=address;
		this.id=id;
		this.long_phone=long_phone;
		this.name=name;
		this.question=question;
		this.short_phone=short_phone;
		this.state=state;
		this.timestamp=timestamp;
		this.type=type;
	}
	public Repair() {
		
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getShort_phone() {
		return short_phone;
	}
	public void setShort_phone(String short_phone) {
		this.short_phone = short_phone;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
