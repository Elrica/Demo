import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Test {
	static String result = "";
	static String token;
/*
	根据帐号密码获取token,返回包含token的json
	利用token授权，请求下面的json信息。


	http://114.215.122.233/repair/v1.0/repair   //获取未接任务。
	http://114.215.122.233/repair/v1.0/receive  //接任务
	http://114.215.122.233/repair/v1.0/myrepair //获取自己接的任务。
	http://114.215.122.233/repair/v1.0/cancel   //取消某个任务，传入json 例：{'id': 1}
	http://114.215.122.233/repair/v1.0/finish   //完成某个任务，传入json 例：{'id': 1}
*/
	public static int connection(String address, String username, String password) throws Exception {
		BufferedReader in;
		URL url = new URL(address);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	//	String author = "Basic " + new BASE64Encoder().encode((username + ":" + password).getBytes());
		String author=null;
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", author);
		conn.connect();
		in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = in.readLine()) != null) {
			result += line;
		}
		
		int JsonData = conn.getResponseCode();
		in.close();
		cut(result);
		initJsonData(JsonData);
		return conn.getResponseCode();
	}

	public static int connection2(String address, String username, String password) throws Exception {
		BufferedReader in;
		URL url = new URL(address);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		//String author = "Basic " + new BASE64Encoder().encode((username + ":" + password).getBytes());
		String author=null;
		conn.setRequestMethod("POST");
		author = author.replace("\r", "");
		author = author.replace("\n", "");
		conn.setRequestProperty("Authorization", author);
		conn.connect();
		in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = in.readLine()) != null) {
			result += line;
		}
		result = convert(result);
		System.out.println(getEncoding(result));
		System.out.println(result);
		in.close();
		// cut(result);
		return conn.getResponseCode();
	}

	public static String getEncoding(String str) {
		String encode = "GB2312";
		try {
			if (str.equals(new String(str.getBytes(encode), encode))) { // 判断是不是GB2312
				String s = encode;
				return s; // 是的话，返回“GB2312“，以下代码同理
			}
		} catch (Exception exception) {
		}
		encode = "ISO-8859-1";
		try {
			if (str.equals(new String(str.getBytes(encode), encode))) { // 判断是不是ISO-8859-1
				String s1 = encode;
				return s1;
			}
		} catch (Exception exception1) {
		}
		encode = "UTF-8";
		try {
			if (str.equals(new String(str.getBytes(encode), encode))) { // 判断是不是UTF-8
				String s2 = encode;
				return s2;
			}
		} catch (Exception exception2) {
		}
		encode = "GBK";
		try {
			if (str.equals(new String(str.getBytes(encode), encode))) { // 判断是不是GBK
				String s3 = encode;
				return s3;
			}
		} catch (Exception exception3) {
		}
		return ""; // 如果都不是，说明输入的内容不属于常见的编码格式。
	}

	public static String cut(String cutter) {
		String[] s = cutter.split("\"");
		for (int i = 0; i < s.length; i++) {
			System.out.println(s[i]);
		}
		token = s[5];
		return s[5];
	}
	


	public static String convert(String utfString) {
		StringBuilder sb = new StringBuilder();
		int i = -1;
		int pos = 0;

		while ((i = utfString.indexOf("\\u", pos)) != -1) {
			sb.append(utfString.substring(pos, i));
			if (i + 5 < utfString.length()) {
				pos = i + 6;
				sb.append((char) Integer.parseInt(utfString.substring(i + 2, i + 6), 16));
			}
		}

		return sb.toString();
	}

	public static void main(String[] args) {
		try {
			System.out.println(connection("http://114.215.122.233/repair/v1.0/token", "123", "123"));
			System.out.println(token);
			System.out.println(connection2("http://114.215.122.233/repair/v1.0/repair", token, ""));
			// connection("http://127.0.0.1:5000/repair/v1.0/","token","");
			// token的请求方法。
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static JsonDataAdapter initJsonData(int JsonData){
		JSONArray jsonArray = new JSONArray(JsonData);
		List<Repair> repairs;
		for(int i=0;i<jsonArray.length();i++){
			JSONObject jsonObject=jsonArray.getJSONObject(i);
			String address = jsonObject.getString("address");
			String id = jsonObject.getString("id");
			String long_phone = jsonObject.getString("long_phone");
			String name = jsonObject.getString("name");
			String question = jsonObject.getString("question");
			String short_phone = jsonObject.getString("short_phone");
			String state = jsonObject.getString("state");
			String timestamp = jsonObject.getString("timestamp");
			String type = jsonObject.getString("type");
			Repair repair=new Repair(address,id,long_phone,name,question,short_phone,state,timestamp,type);
			repairs = new ArrayList<Repair>();
			repairs.add(repair);	
		}
	}
	
}
